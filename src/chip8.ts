import "regenerator-runtime/runtime";
import {
    Program,
    Screen,
    ArgumentType,
    DecodedInstruction
} from "./types";
import { instructions } from "./instructions";
import { formatHex, getPixel } from "./util";

const PC_START = 0x200;
const SCREEN_SCALE = 10;
const FONTSET_ADDRESS = 0x00;
const OFF_COLOR = "#000000";
const ON_COLOR = "#00FF00";

const KeyMap = {
    // 1
    49: 0x1,
    // 2
    50: 0x2,
    // 3
    51: 0x3,
    // 4
    52: 0xC,
    // Q
    81: 0x4,
    // W
    87: 0x5,
    // E
    69: 0x6,
    // R
    82: 0xD,
    // A
    65: 0x7,
    // S
    83: 0x8,
    // D
    68: 0x9,
    // F
    70: 0xE,
    // Z
    90: 0xA,
    // X
    88: 0x0,
    // C
    67: 0xB,
    // V
    86: 0xF
};

const FontSet = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
];

// Decode instruction and arguments from opcode
const decodeInstruction = (program: Program): DecodedInstruction => {
    // Find instruction
    const opcode = program.memory[program.pc] << 8 | program.memory[program.pc + 1];
    const instruction = instructions.find((i) => (opcode & i.mask) === i.pattern);
    if (!instruction) {
        throw new Error(
            `Unknown instruction "${formatHex(opcode)}" at address "${formatHex(program.pc)}"`
        );
    }
    // Decode arguments
    const args = instruction.arguments.map((arg) => {
        switch (arg) {
            case ArgumentType.X:
                return (opcode >> 8) & 0x000F;
            case ArgumentType.Y:
                return (opcode >> 4) & 0x000F;
            case ArgumentType.N:
                return opcode & 0x000F;
            case ArgumentType.KK:
                return opcode & 0x00FF;
            case ArgumentType.NNN:
                return opcode & 0x0FFF;
            default:
                throw new Error(`Unknown argument type "${arg}"`);
        }
    });
    const decoded: DecodedInstruction = {
        name: instruction.name,
        incrementPC: instruction.incrementPC,
        args,
        fn: instruction.fn
    };

    console.log(
        `%c[${formatHex(program.pc)}][${decoded.name}] %c${decoded.args.join(", ")}`,
        "color: green",
        "color: white"
    );

    return decoded;
};

// Fetch rom file and load into memory
const loadROM = async (filename: string, program: Program) => {
    console.log(`Loading rom "${filename}"`);
    const response = await fetch(filename);
    const rom = new Uint8Array(await response.arrayBuffer());
    for (let i = 0; i < rom.length; i++) {
        program.memory[PC_START + i] = rom[i];
    }
};

// Draw screen
const draw = (screen: Screen) => {
    screen.context.fillStyle = OFF_COLOR;
    screen.context.fillRect(0, 0, 64 * SCREEN_SCALE, 32 * SCREEN_SCALE);
    for (let x = 0; x < 64; x++) {
        for (let y = 0; y < 32; y++) {
            const pixel = getPixel(screen.buffer, x, y);
            if (pixel) {
                screen.context.fillStyle = ON_COLOR;
                screen.context.fillRect(x * SCREEN_SCALE, y * SCREEN_SCALE, SCREEN_SCALE, SCREEN_SCALE);
            }
        }
    }
};

// Init debug panel
const initDebugPanel = () => {
    const $table = document.getElementById("debug-registers");

    const $pcDiv = document.createElement("div");
    $pcDiv.className = "register";
    const $pcHeaderDiv = document.createElement("div");
    $pcHeaderDiv.className = "register-header";
    $pcHeaderDiv.innerText = "PC";
    $pcDiv.appendChild($pcHeaderDiv);
    const $pcBodyDiv = document.createElement("div");
    $pcBodyDiv.className = "register-body";
    $pcBodyDiv.innerText = "0x200";
    $pcDiv.appendChild($pcBodyDiv);
    $table.appendChild($pcDiv);

    const $iDiv = document.createElement("div");
    $iDiv.className = "register";
    const $iHeaderDiv = document.createElement("div");
    $iHeaderDiv.className = "register-header";
    $iHeaderDiv.innerText = "I";
    $iDiv.appendChild($iHeaderDiv);
    const $iBodyDiv = document.createElement("div");
    $iBodyDiv.className = "register-body";
    $iBodyDiv.innerText = "0x0000";
    $iDiv.appendChild($iBodyDiv);
    $table.appendChild($iDiv);

    const $spDiv = document.createElement("div");
    $spDiv.className = "register";
    const $spHeaderDiv = document.createElement("div");
    $spHeaderDiv.className = "register-header";
    $spHeaderDiv.innerText = "SP";
    $spDiv.appendChild($spHeaderDiv);
    const $spBodyDiv = document.createElement("div");
    $spBodyDiv.className = "register-body";
    $spBodyDiv.innerText = "0x0000";
    $spDiv.appendChild($spBodyDiv);
    $table.appendChild($spDiv);

    const $dtDiv = document.createElement("div");
    $dtDiv.className = "register";
    const $dtHeaderDiv = document.createElement("div");
    $dtHeaderDiv.className = "register-header";
    $dtHeaderDiv.innerText = "DT";
    $dtDiv.appendChild($dtHeaderDiv);
    const $dtBodyDiv = document.createElement("div");
    $dtBodyDiv.className = "register-body";
    $dtBodyDiv.innerText = "0";
    $dtDiv.appendChild($dtBodyDiv);
    $table.appendChild($dtDiv);

    const $stDiv = document.createElement("div");
    $stDiv.className = "register";
    const $stHeaderDiv = document.createElement("div");
    $stHeaderDiv.className = "register-header";
    $stHeaderDiv.innerText = "ST";
    $stDiv.appendChild($stHeaderDiv);
    const $stBodyDiv = document.createElement("div");
    $stBodyDiv.className = "register-body";
    $stBodyDiv.innerText = "0";
    $stDiv.appendChild($stBodyDiv);
    $table.appendChild($stDiv);

    for (let i = 0; i < 16; i++) {
        const $registerDiv = document.createElement("div");
        $registerDiv.className = "register";
        const $headerDiv = document.createElement("div");
        $headerDiv.className = "register-header";
        $headerDiv.innerText = `V${i}`;
        $registerDiv.appendChild($headerDiv);
        const $bodyDiv = document.createElement("div");
        $bodyDiv.className = "register-body";
        $bodyDiv.innerText = "0x00";
        $registerDiv.appendChild($bodyDiv);
        $table.appendChild($registerDiv);
    }
};

// Update debug panel
const updateDebugPanel = (program: Program) => {
    const $table = document.getElementById("debug-registers");
    const $bodyDivs: NodeListOf<HTMLElement> = $table.querySelectorAll(".register-body");

    $bodyDivs[0].innerText = formatHex(program.pc, 4);
    $bodyDivs[1].innerText = formatHex(program.i, 4);
    $bodyDivs[2].innerText = formatHex(program.sp, 4);
    $bodyDivs[3].innerText = program.dt.toString();
    $bodyDivs[4].innerText = program.st.toString();

    for (let i = 0; i < 16; i++) {
        $bodyDivs[i + 5].innerText = formatHex(program.registers[i], 2);
    }
};

// Start program
const start = async (filename: string) => {
    const canvas = <HTMLCanvasElement>document.getElementById("screen");
    canvas.width = 64 * SCREEN_SCALE;
    canvas.height = 32 * SCREEN_SCALE;

    const program: Program = {
        memory: new Uint8Array(4096),
        registers: new Uint8Array(16),
        stack: new Uint16Array(16),
        pc: PC_START,
        i: 0,
        sp: 0,
        dt: 0,
        st: 0,
        keys: new Uint8Array(16),
        running: true
    };
    const screen: Screen = {
        context: canvas.getContext("2d"),
        buffer: new Uint8Array(2048),
        dirty: true
    };

    document.addEventListener("keydown", (event) => {
        event.preventDefault();
        if (KeyMap.hasOwnProperty(event.keyCode)) {
            const key = KeyMap[event.keyCode];
            program.keys[key] = 1;
            console.log(
                `%c[Key Down] %c${formatHex(key)}`,
                "color: red",
                "color: white"
            );
        }
    });

    document.addEventListener("keyup", (event) => {
        event.preventDefault();
        if (KeyMap.hasOwnProperty(event.keyCode)) {
            const key = KeyMap[event.keyCode];
            program.keys[key] = 0;
            console.log(
                `%c[Key Up] %c${formatHex(key)}`,
                "color: red",
                "color: white"
            );
        }
    });

    for (let i = 0; i < 80; i++) {
        program.memory[FONTSET_ADDRESS + i] = FontSet[i];
    }

    await loadROM(filename, program);

    initDebugPanel();

    const step = async () => {
        const instruction = decodeInstruction(program);

        try {
            await instruction.fn(instruction.args, program, screen);
        } catch (error) {
            console.error(`Exception in "${instruction.name}": ${error}`);
            if (error.stack) {
                console.error(error.stack);
            }
        }

        if (screen.dirty) {
            draw(screen);
            screen.dirty = false;
        }

        // Decrement timers
        if (program.dt > 0) {
            program.dt--;
        }
        if (program.st > 0) {
            program.st--;
            if (program.st === 0) {
                // TODO: Play sound
                console.log("beep");
            }
        }

        // Increment program counter
        if (instruction.incrementPC) {
            program.pc += 2;
        }

        updateDebugPanel(program);

        if (program.running) {
            requestAnimationFrame(step);
        }
    };

    console.log("Starting program");
    requestAnimationFrame(step);
};

start("/roms/Breakout [Carmelo Cortez, 1979].ch8");