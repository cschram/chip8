import { Program, Screen, Instruction } from "./types";
import { instructions } from "./instructions";

const PC_START = 0x200;

describe("CPU Instructions", () => {
    let program: Program;
    let screen: Screen;

    const callInstruction = (name: string, args: number[]) => {
        const inst = instructions.find((inst) => inst.name === name);
        inst.fn(args, program, screen);
        if (inst.incrementPC) {
            program.pc += 2;
        }
    };

    beforeEach(() => {
        program = {
            memory: new Uint8Array(4096),
            registers: new Uint8Array(16),
            stack: new Uint16Array(16),
            pc: PC_START,
            i: 0,
            sp: 0,
            dt: 0,
            st: 0,
            keys: new Uint8Array(16),
            running: true
        };
        screen = {
            context: null,
            buffer: new Uint8Array(2048),
            dirty: true
        };
    });

    test("CLS", () => {
        // Fill screen buffer with "on" pixels
        for (let i = 0; i < 2048; i++) {
            screen.buffer[i] = 1;
        }

        callInstruction("CLS", []);

        // Check that screen buffer has been cleared
        for (let i = 0; i < 2048; i++) {
            expect(screen.buffer[i]).toBe(0);
        }
    });

    test("RET", () => {
        program.stack[0] = 0x400;
        program.sp = 1;
        callInstruction("RET", []);
        expect(program.pc).toBe(0x402);
    });

    test("JP addr", () => {
        callInstruction("JP addr", [0x400]);
        expect(program.pc).toBe(0x400);
    });

    test("CALL addr", () => {
        callInstruction("CALL addr", [0x400]);
        expect(program.stack[0]).toBe(PC_START);
        expect(program.sp).toBe(1);
        expect(program.pc).toBe(0x400);
    });

    test("SE Vx, byte", () => {
        program.registers[0] = 0xF;
        callInstruction("SE Vx, byte", [0, 0]);
        expect(program.pc).toBe(PC_START + 2);

        program.pc = PC_START;
        callInstruction("SE Vx, byte", [0, 0xF]);
        expect(program.pc).toBe(PC_START + 4);
    });

    test("SNE Vx, byte", () => {
        program.registers[0] = 0xF;
        callInstruction("SNE Vx, byte", [0, 0xF]);
        expect(program.pc).toBe(PC_START + 2);

        program.pc = PC_START;
        callInstruction("SNE Vx, byte", [0, 0]);
        expect(program.pc).toBe(PC_START + 4);
    });

    test("SE Vx, Vy", () => {
        program.registers[0] = 0;
        program.registers[1] = 0xF;
        callInstruction("SE Vx, Vy", [0, 1]);
        expect(program.pc).toBe(PC_START + 2);

        program.pc = PC_START;
        program.registers[0] = 0xF;
        callInstruction("SE Vx, Vy", [0, 1]);
        expect(program.pc).toBe(PC_START + 4);
    });

    test("LD Vx, byte", () => {
        callInstruction("LD Vx, byte", [0, 0xF]);
        expect(program.registers[0]).toBe(0xF);
    });

    test("ADD Vx, byte", () => {
        program.registers[0] = 0xA;
        callInstruction("ADD Vx, byte", [0, 0xA]);
        expect(program.registers[0]).toBe(0x14);
    });

    test("LD Vx, Vy", () => {
        program.registers[1] = 0xF;
        callInstruction("LD Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0xF);
    });

    test("OR Vx, Vy", () => {
        program.registers[0] = 0xF0;
        program.registers[1] = 0x0F;
        callInstruction("OR Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0xFF);
    });

    test("AND Vx, Vy", () => {
        program.registers[0] = 0xFF;
        program.registers[1] = 0xF0;
        callInstruction("AND Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0xF0);
    });

    test("XOR Vx, Vy", () => {
        program.registers[0] = 0xA;
        program.registers[1] = 0xF;
        callInstruction("XOR Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0x5);
    });

    test("ADD Vx, Vy", () => {
        program.registers[0] = 0xA;
        program.registers[1] = 0xA;
        callInstruction("ADD Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0x14);
        expect(program.registers[0xF]).toBe(0);

        program.registers[0] = 0xAA;
        program.registers[1] = 0xAA;
        callInstruction("ADD Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0x54);
        expect(program.registers[0xF]).toBe(1);
    });

    test("SUB Vx, Vy", () => {
        program.registers[0] = 0xF;
        program.registers[1] = 0xA;
        callInstruction("SUB Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(5);
        expect(program.registers[0xF]).toBe(0);

        program.registers[0] = 0xA;
        program.registers[1] = 0xF;
        callInstruction("SUB Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0xFB);
        expect(program.registers[0xF]).toBe(1);
    });

    test("SHR Vx {, Vy}", () => {
        program.registers[0] = 0xF;
        callInstruction("SHR Vx {, Vy}", [0]);
        expect(program.registers[0]).toBe(7);
        expect(program.registers[0xF]).toBe(1);
    });

    test("SUBN Vx, Vy", () => {
        program.registers[0] = 0xA;
        program.registers[1] = 0xF;
        callInstruction("SUBN Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(5);
        expect(program.registers[0xF]).toBe(0);

        program.registers[0] = 0xF;
        program.registers[1] = 0xA;
        callInstruction("SUBN Vx, Vy", [0, 1]);
        expect(program.registers[0]).toBe(0xFB);
        expect(program.registers[0xF]).toBe(1);
    });

    test("SHL Vx {, Vy}", () => {
        program.registers[0] = 0x1;
        callInstruction("SHL Vx {, Vy}", [0, 1]);
        expect(program.registers[0xF]).toBe(0);
        expect(program.registers[0]).toBe(2);

        program.registers[0] = 0xFF;
        callInstruction("SHL Vx {, Vy}", [0, 1]);
        expect(program.registers[0xF]).toBe(1);
        expect(program.registers[0]).toBe(254);
    });

    test("SNE Vx, Vy", () => {
        program.registers[0] = 1;
        program.registers[1] = 1;
        callInstruction("SNE Vx, Vy", [0, 1]);
        expect(program.pc).toBe(PC_START + 2);

        program.pc = PC_START;
        program.registers[0] = 1;
        program.registers[1] = 2;
        callInstruction("SNE Vx, Vy", [0, 1]);
        expect(program.pc).toBe(PC_START + 4);
    });

    test("LD I, addr", () => {
        callInstruction("LD I, addr", [0xF]);
        expect(program.i).toBe(0xF);
    });

    test("JP V0, addr", () => {
        program.registers[0] = 0xAA;
        callInstruction("JP V0, addr", [0xFF]);
        expect(program.pc).toBe(0x1A9);
    });

    test("SKP Vx", () => {
        program.keys[0] = 1;
        program.registers[0] = 0;
        callInstruction("SKP Vx", [0]);
        expect(program.pc).toBe(PC_START + 4);

        program.pc = PC_START;
        program.keys[0] = 0;
        program.registers[0] = 0;
        callInstruction("SKP Vx", [0]);
        expect(program.pc).toBe(PC_START + 2);
    });

    test("SKNP Vx", () => {
        program.keys[0] = 1;
        program.registers[0] = 0;
        callInstruction("SKNP Vx", [0]);
        expect(program.pc).toBe(PC_START + 2);

        program.pc = PC_START;
        program.keys[0] = 0;
        program.registers[0] = 0;
        callInstruction("SKNP Vx", [0]);
        expect(program.pc).toBe(PC_START + 4);
    });

    test("LD Vx, DT", () => {
        program.dt = 0xF;
        callInstruction("LD Vx, DT", [0]);
        expect(program.registers[0]).toBe(0xF);
    });

    test("LD Vx, K", () => {
        callInstruction("LD Vx, K", [0]);
        expect(program.registers[0]).toBe(0);

        program.keys[1] = 1;
        callInstruction("LD Vx, K", [0]);
        expect(program.registers[0]).toBe(1);
    });

    test("LD DT, Vx", () => {
        program.registers[0] = 0xF;
        callInstruction("LD DT, Vx", [0]);
        expect(program.dt).toBe(0xF);
    });

    test("LD ST, Vx", () => {
        program.registers[0] = 0xF;
        callInstruction("LD ST, Vx", [0]);
        expect(program.st).toBe(0xF);
    });

    test("ADD I, Vx", () => {
        program.i = 0xF;
        program.registers[0] = 0xF;
        callInstruction("ADD I, Vx", [0]);
        expect(program.i).toBe(0x1E);
        expect(program.registers[0xF]).toBe(0);

        program.i = 0xFFF;
        program.registers[0] = 0xF;
        callInstruction("ADD I, Vx", [0]);
        expect(program.i).toBe(0x100E);
        expect(program.registers[0xF]).toBe(1);
    });

    test("LD F, Vx", () => {
        program.registers[0] = 2;
        callInstruction("LD F, Vx", [0]);
        expect(program.i).toBe(10);
    });

    test("LD B, Vx", () => {
        program.registers[0] = 0xFF;
        callInstruction("LD B, Vx", [0]);
        expect(program.memory[0]).toBe(2);
        expect(program.memory[1]).toBe(5);
        expect(program.memory[2]).toBe(5);
    });

    test("LD [I], Vx", () => {
        for (let i = 0; i < 16; i++) {
            program.registers[i] = i;
        }
        callInstruction("LD [I], Vx", [15]);
        expect(program.i).toBe(16);
        for (let i = 0; i < 16; i++) {
            expect(program.memory[i]).toBe(i);
        }
    });

    test("LD Vx, [I]", () => {
        for (let i = 0; i < 16; i++) {
            program.memory[i] = i;
        }
        callInstruction("LD Vx, [I]", [15]);
        expect(program.i).toBe(16);
        for (let i = 0; i < 16; i++) {
            expect(program.registers[i]).toBe(i);
        }
    });
});