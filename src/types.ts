// Main program state
export interface Program {
    // System memory (4KB)
    memory: Uint8Array;
    // CPU Registers
    registers: Uint8Array;
    // CPU Stack
    stack: Uint16Array;
    // Program Counter
    pc: number;
    // Index Register
    i: number;
    // Stack Pointer
    sp: number;
    // Delay Timer
    dt: number;
    // Sound Timer
    st: number;
    // Key state
    keys: Uint8Array;
    // Running flag
    running: boolean;
}

// Screen state
export interface Screen {
    context: CanvasRenderingContext2D;
    // Screen buffer 64x32 array of boolean values
    buffer: Uint8Array;
    // Dirty flag, redraw when true
    dirty: boolean;
}

export enum ArgumentType {
    X,
    Y,
    N,
    KK,
    NNN
}

export interface Instruction {
    name: string;
    mask: number;
    pattern: number;
    arguments: Array<ArgumentType>;
    incrementPC: boolean;
    fn: (args: Array<number>, program: Program, screen: Screen) => void;
}

export interface DecodedInstruction {
    name: string;
    incrementPC: boolean;
    args: Array<number>;
    fn: (args: Array<number>, program: Program, screen: Screen) => void;
}