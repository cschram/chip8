import { Instruction, ArgumentType } from "./types";
import { rand, getPixel, setPixel } from "./util";

const FONTSET_BYTES_PER_CHAR = 5;

export const instructions: Array<Instruction> = [
    {
        name: "CLS",
        mask: 0x00F0,
        pattern: 0x00E0,
        arguments: [],
        incrementPC: true,
        fn(args, program, screen) {
            for (let i = 0; i < 2048; i++) {
                screen.buffer[i] = 0;
            }
            screen.dirty = true;
        }
    },
    {
        name: "RET",
        mask: 0x00FF,
        pattern: 0x00EE,
        arguments: [],
        incrementPC: true,
        fn(args, program, screen) {
            program.pc = program.stack[--program.sp];
        }
    },
    {
        name: "JP addr",
        mask: 0xF000,
        pattern: 0x1000,
        arguments: [ArgumentType.NNN],
        incrementPC: false,
        fn(args, program, screen) {
            const [nnn] = args;
            program.pc = nnn;
        }
    },
    {
        name: "CALL addr",
        mask: 0xF000,
        pattern: 0x2000,
        arguments: [ArgumentType.NNN],
        incrementPC: false,
        fn(args, program, screen) {
            const [nnn] = args;
            program.stack[program.sp] = program.pc;
            program.sp++;
            program.pc = nnn;
        }
    },
    {
        name: "SE Vx, byte",
        mask: 0xF000,
        pattern: 0x3000,
        arguments: [ArgumentType.X, ArgumentType.KK],
        incrementPC: false,
        fn(args, program, screen) {
            const [x, kk] = args;
            program.pc += (program.registers[x] === kk) ? 4 : 2;
        }
    },
    {
        name: "SNE Vx, byte",
        mask: 0xF000,
        pattern: 0x4000,
        arguments: [ArgumentType.X, ArgumentType.KK],
        incrementPC: false,
        fn(args, program, screen) {
            const [x, kk] = args;
            program.pc += (program.registers[x] === kk) ? 2 : 4;
        }
    },
    {
        name: "SE Vx, Vy",
        mask: 0xF000,
        pattern: 0x5000,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: false,
        fn(args, program, screen) {
            const [x, y] = args;
            program.pc += program.registers[x] === program.registers[y] ? 4 : 2;
        }
    },
    {
        name: "LD Vx, byte",
        mask: 0xF000,
        pattern: 0x6000,
        arguments: [ArgumentType.X, ArgumentType.KK],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, kk] = args;
            program.registers[x] = kk;
        }
    },
    {
        name: "ADD Vx, byte",
        mask: 0xF000,
        pattern: 0x7000,
        arguments: [ArgumentType.X, ArgumentType.KK],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, kk] = args;
            program.registers[x] += kk;
        }
    },
    {
        name: "LD Vx, Vy",
        mask: 0xF000,
        pattern: 0x8000,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[x] = program.registers[y];
        }
    },
    {
        name: "OR Vx, Vy",
        mask: 0xF00F,
        pattern: 0x8001,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[x] |= program.registers[y];
        }
    },
    {
        name: "AND Vx, Vy",
        mask: 0xF00F,
        pattern: 0x8002,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[x] &= program.registers[y];
        }
    },
    {
        name: "XOR Vx, Vy",
        mask: 0xF00F,
        pattern: 0x8003,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[x] ^= program.registers[y];
        }
    },
    {
        name: "ADD Vx, Vy",
        mask: 0xF00F,
        pattern: 0x8004,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[0xF] = (program.registers[x] + program.registers[y]) > 255 ? 1 : 0;
            program.registers[x] += program.registers[y];
        }
    },
    {
        name: "SUB Vx, Vy",
        mask: 0xF00F,
        pattern: 0x8005,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[0xF] = program.registers[x] > program.registers[y] ? 0 : 1;
            program.registers[x] -= program.registers[y];
        }
    },
    {
        name: "SHR Vx {, Vy}",
        mask: 0xF00F,
        pattern: 0x8006,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, _] = args;
            program.registers[0xF] = program.registers[x] & 0x1;
            program.registers[x] = program.registers[x] >> 1;
        }
    },
    {
        name: "SUBN Vx, Vy",
        mask: 0xF00F,
        pattern: 0x8007,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y] = args;
            program.registers[0xF] = program.registers[y] > program.registers[x] ? 0 : 1;
            program.registers[x] = program.registers[y] - program.registers[x];
        }
    },
    {
        name: "SHL Vx {, Vy}",
        mask: 0xF00F,
        pattern: 0x800E,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, _] = args;
            program.registers[0xF] = (program.registers[x] >> 7) & 0x1;
            program.registers[x] = program.registers[x] << 1;
        }
    },
    {
        name: "SNE Vx, Vy",
        mask: 0xF000,
        pattern: 0x9000,
        arguments: [ArgumentType.X, ArgumentType.Y],
        incrementPC: false,
        fn(args, program, screen) {
            const [x, y] = args;
            const vx = program.registers[x];
            const vy = program.registers[y];
            program.pc += vx !== vy ? 4 : 2;
        }
    },
    {
        name: "LD I, addr",
        mask: 0xF000,
        pattern: 0xA000,
        arguments: [ArgumentType.NNN],
        incrementPC: true,
        fn(args, program, screen) {
            const [nnn] = args;
            program.i = nnn;
        }
    },
    {
        name: "JP V0, addr",
        mask: 0xF000,
        pattern: 0xB000,
        arguments: [ArgumentType.NNN],
        incrementPC: false,
        fn(args, program, screen) {
            const [nnn] = args;
            program.pc = program.registers[0] + nnn;
        }
    },
    {
        name: "RND Vx, byte",
        mask: 0xF000,
        pattern: 0xC000,
        arguments: [ArgumentType.X, ArgumentType.KK],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, kk] = args;
            program.registers[x] = rand(0, 255) & kk;
            console.log(
                `%c[RNG] %c${program.registers[x]}`,
                "color: red",
                "color: white"
            );
        }
    },
    {
        name: "DRW Vx, Vy, n",
        mask: 0xF000,
        pattern: 0xD000,
        arguments: [ArgumentType.X, ArgumentType.Y, ArgumentType.N],
        incrementPC: true,
        fn(args, program, screen) {
            const [x, y, n] = args;
            const vx = program.registers[x];
            const vy = program.registers[y];
            program.registers[0xF] = 0;
            for (let byteIndex = 0; byteIndex < n; byteIndex++) {
                const byte = program.memory[program.i + byteIndex];
                for (let bitIndex = 0; bitIndex < 8; bitIndex++) {
                    const bit = (byte >> bitIndex) & 0x1;
                    const sx = (vx + (7 - bitIndex)) % 64;
                    const sy = (vy + byteIndex) % 32;
                    const pixel = getPixel(screen.buffer, sx, sy);
                    if (bit && pixel) {
                        program.registers[0xF] = 1;
                    }
                    setPixel(screen.buffer, sx, sy, pixel ^ bit);
                }
            }
            screen.dirty = true;
        }
    },
    {
        name: "SKP Vx",
        mask: 0xF0FF,
        pattern: 0xE09E,
        arguments: [ArgumentType.X],
        incrementPC: false,
        fn(args, program, screen) {
            const [x] = args;
            const vx = program.registers[x];
            program.pc += program.keys[vx] ? 4 : 2;
        }
    },
    {
        name: "SKNP Vx",
        mask: 0xF0FF,
        pattern: 0xE0A1,
        arguments: [ArgumentType.X],
        incrementPC: false,
        fn(args, program, screen) {
            const [x] = args;
            const vx = program.registers[x];
            program.pc += program.keys[vx] ? 2 : 4;
        }
    },
    {
        name: "LD Vx, DT",
        mask: 0xF0FF,
        pattern: 0xF007,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            program.registers[x] = program.dt;
        }
    },
    {
        name: "LD Vx, K",
        mask: 0xF0FF,
        pattern: 0xF00A,
        arguments: [ArgumentType.X],
        incrementPC: false,
        fn(args, program, screen) {
            const [x] = args;
            for (let i = 0; i < 16; i++) {
                if (program.keys[i]) {
                    program.registers[x] = i;
                    program.pc += 2;
                    return;
                }
            }
        }
    },
    {
        name: "LD DT, Vx",
        mask: 0xF0FF,
        pattern: 0xF015,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            program.dt = program.registers[x];
        }
    },
    {
        name: "LD ST, Vx",
        mask: 0xF0FF,
        pattern: 0xF018,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            program.st = program.registers[x];
        }
    },
    {
        name: "ADD I, Vx",
        mask: 0xF0FF,
        pattern: 0xF01E,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            const vx = program.registers[x];
            program.registers[0xF] = (program.i + vx) > 0xFFF ? 1 : 0;
            program.i += vx;
        }
    },
    {
        name: "LD F, Vx",
        mask: 0xF0FF,
        pattern: 0xF029,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            program.i = FONTSET_BYTES_PER_CHAR * program.registers[x];
        }
    },
    {
        name: "LD B, Vx",
        mask: 0xF0FF,
        pattern: 0xF033,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            const vx = program.registers[x];
            program.memory[program.i] = vx / 100;
            program.memory[program.i + 1] = (vx / 10) % 10;
            program.memory[program.i + 2] = (vx % 100) % 10;
        }
    },
    {
        name: "LD [I], Vx",
        mask: 0xF0FF,
        pattern: 0xF055,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            for (let i = 0; i <= x; i++) {
                program.memory[program.i + i] = program.registers[i];
            }
            program.i += x + 1;
        }
    },
    {
        name: "LD Vx, [I]",
        mask: 0xF0FF,
        pattern: 0xF065,
        arguments: [ArgumentType.X],
        incrementPC: true,
        fn(args, program, screen) {
            const [x] = args;
            for (let i = 0; i <= x; i++) {
                program.registers[i] = program.memory[program.i + i];
            }
            program.i += x + 1;
        }
    }
];