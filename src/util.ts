export const formatHex = (value: number, minChars?: number): string => {
    if (isNaN(value)) {
        return "0x????";
    }
    const hex = value.toString(16).toUpperCase();
    let str = "0x";
    if (minChars && hex.length < minChars) {
        for (let i = 0; i < (minChars - hex.length); i++) {
            str += "0";
        }
    }
    return `${str}${hex}`;
}

export const rand = (min: number, max: number): number => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const getPixel = (buffer: Uint8Array, x: number, y: number): number => {
    return buffer[(y * 64) + x];
};

export const setPixel = (buffer: Uint8Array, x: number, y: number, value: number) => {
    buffer[(y * 64) + x] = value;
};