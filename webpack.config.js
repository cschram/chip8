const webpack = require("webpack");
const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

let config = {
    mode: process.env.NODE_ENV === "production" ? "production" : "development",
    entry: path.resolve(__dirname, `src/chip8.ts`),
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "chip8.min.js"
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: "babel-loader"
                    },
                    {
                        loader: "ts-loader"
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".js", ".json"]
    },
    plugins: [
        new webpack.EnvironmentPlugin(["NODE_ENV"])
    ]
};

if (process.env.NODE_ENV === "development") {
    config.devServer = {
        port: 8000
    };
    config.devtool = "inline-source-map";
    config.plugins.push(new webpack.HotModuleReplacementPlugin());
} else {
    config.plugins.push(new CopyWebpackPlugin(["index.html"]));
}

module.exports = config;